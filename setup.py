import os
import re
import sys


#curl --request GET --header 'PRIVATE-TOKEN: ' 'https://gitlab.com/api/v4/projects/31768936/repository/files/setup%2Epy/raw?ref=main' > setup.py && python setup.py ngrok_token fleetURL

class setup:

    #Testing Values
    #ngrok = "1212odnodosoidsa"
    #url = "http://api.open-notify.org/astros.json"

    ngrok = sys.argv[1]
    print("Got the ngrok data")
    url = sys.argv[2]
    print("Got url data")
    

    def run(self):
        self.ngrok_check()
        self.url_save()
        self.delete_file()

    def ngrok_set(self):
        with open(os.path.expanduser("~/.ngrok2/ngrok.yml"),'w') as file:
            s = "authtoken: " + self.ngrok + "\n"
            file.write(s)
        file.close()

        with open(os.path.expanduser("~/.ngrok2/tcp_ngrok.yml"),'w') as file:
            strng = self.data
            strng[0] = "authtoken: " + self.ngrok +"\n"
            file.writelines(strng)

        file.close()

        print("Ngrok Files written successfully")
    
    def ngrok_check(self):
        with open(os.path.expanduser("~/.ngrok2/tcp_ngrok.yml"),'r') as f :
               strng = f.readlines()
               #print(strng)
               s = re.split(" |\n",strng[0])
               if s[1] == self.ngrok :
                   print("NgrokId already Set")
               else:
                   self.data = strng
                   self.ngrok_set()
        f.close()

    def url_save(self):
        with open(os.path.expanduser("~/.urlconfig"),'w') as file:
            s = "URL: " + self.url
            file.write(s)

        file.close()
        print("URL Saved")

    def delete_file(self):
        os.remove("setup.py")

obj = setup()
obj.run()
